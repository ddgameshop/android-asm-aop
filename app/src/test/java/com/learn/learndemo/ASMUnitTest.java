package com.learn.learndemo;

public class ASMUnitTest {

//    @Test
//    public void test(){
//        try {
//            FileInputStream fis = new FileInputStream("/Users/chendennies/Documents/022kotlin/LearnDemo/app/build/intermediates/javac/debugUnitTest/classes/com/learn/learndemo/InjectTest.class");
//            ClassReader classReader = new ClassReader(fis);
//
//            ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
//            classReader.accept(new MyClassVisitor(Opcodes.ASM7, classWriter), ClassReader.EXPAND_FRAMES);
//
//            byte[] bytes = classWriter.toByteArray();
//            FileOutputStream fos = new FileOutputStream("/Users/chendennies/Documents/022kotlin/LearnDemo/app/build/intermediates/javac/debugUnitTest/classes/com/learn/learndemo/InjectTest2.class");
//            fos.write(bytes);
//
//            fis.close();
//            fos.close();
//
////            InjectTest injectTest = new InjectTest();
////            injectTest.method();
//        } catch (Exception E) {
//            System.out.println("======" + E.toString());
//        }
//
//
//    }
//
//    class MyClassVisitor extends ClassVisitor {
//        public MyClassVisitor(int api, ClassVisitor classVisitor) {
//            super(api, classVisitor);
//        }
//
//        @Override
//        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
//            MethodVisitor methodVisitor = super.visitMethod(access, name, descriptor, signature, exceptions);
//            System.out.println("method name=" + name + ",descriptor=" + descriptor + ",signature=" + signature);
//            return new MyMethodVisitor(api, methodVisitor, access, name, descriptor);
//
//        }
//    }
//
//    class MyMethodVisitor extends AdviceAdapter {
//        protected MyMethodVisitor(int api, MethodVisitor methodVisitor, int access, String name, String descriptor) {
//            super(api, methodVisitor, access, name, descriptor);
//        }
//
//        int start;
//        int end;
//        @Override
//        protected void onMethodEnter() {
//            super.onMethodEnter();
//            if(!isNeedHotpot) {
//                return;
//            }
////            INVOKESTATIC java/lang/System.currentTimeMillis
////            LSTORE 1
//            invokeStatic(Type.getType("Ljava/lang/System;"), new Method("currentTimeMillis",  "()J"));
//            start = newLocal(Type.LONG_TYPE);
//            storeLocal(start);
//
//        }
//
//        @Override
//        protected void onMethodExit(int opcode) {
//            super.onMethodExit(opcode);
//            if(!isNeedHotpot) {
//                return;
//            }
////            INVOKESTATIC java/lang/System.currentTimeMillis ()J
////            LSTORE 3
//
//            invokeStatic(Type.getType("Ljava/lang/System;"), new Method("currentTimeMillis", "()J"));
//            end = newLocal(Type.LONG_TYPE);
//            storeLocal(end);
//
////            GETSTATIC java/lang/System.out : Ljava/io/PrintStream;
////            NEW java/lang/StringBuilder
////            DUP
////            INVOKESPECIAL java/lang/StringBuilder.<init> ()V
////            LDC "executed time="
////            INVOKEVIRTUAL java/lang/StringBuilder.append (Ljava/lang/String;)Ljava/lang/StringBuilder;
////            LLOAD 3
////            LLOAD 1
////            LSUB
////            INVOKEVIRTUAL java/lang/StringBuilder.append (J)Ljava/lang/StringBuilder;
////            LDC "ms"
////            INVOKEVIRTUAL java/lang/StringBuilder.append (Ljava/lang/String;)Ljava/lang/StringBuilder;
////            INVOKEVIRTUAL java/lang/StringBuilder.toString ()Ljava/lang/String;
////            INVOKEVIRTUAL java/io/ PrintStream.println (Ljava/lang/String;)V
//
//            getStatic(Type.getType("Ljava/lang/System;"), "out", Type.getType("Ljava/io/PrintStream;"));
//            newInstance(Type.getType("Ljava/lang/StringBuilder;"));
//            dup();
//            invokeConstructor(Type.getType("Ljava/lang/StringBuilder;"), new Method("<init>", "()V"));
//            visitLdcInsn("executed time=");
//            invokeVirtual(Type.getType("Ljava/lang/StringBuilder;"), new Method("append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;"));
//            loadLocal(end);
//            loadLocal(start);
//            math(LSUB, Type.LONG_TYPE);
//            invokeVirtual(Type.getType("Ljava/lang/StringBuilder;"), new Method("append", "(J)Ljava/lang/StringBuilder;"));
//            visitLdcInsn("ms");
//            invokeVirtual(Type.getType("Ljava/lang/StringBuilder;"), new Method("append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;"));
//            invokeVirtual(Type.getType("Ljava/lang/StringBuilder;"), new Method("toString", "()Ljava/lang/String;"));
//            invokeVirtual(Type.getType("Ljava/io/PrintStream;"), new Method("println", "(Ljava/lang/String;)V"));
//        }
//
//        private boolean isNeedHotpot = false;
//        @Override
//        public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
//            System.out.println(" visitAnnotation descriptor=" + descriptor);
//            if("Lcom/learn/learndemo/ASMTime;".equals(descriptor)) {
//                isNeedHotpot = true;
//            }
//            return super.visitAnnotation(descriptor, visible);
//        }
//    }
}

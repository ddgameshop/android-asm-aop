package com.learn.learndemo;

import org.junit.Test;

public class InjectTest {

    @Test
    public static void main(String[] args) throws InterruptedException{
        Thread.sleep(3000);
        method();
    }

    @ASMTime
    public static void method() throws InterruptedException{
//        long start = System.currentTimeMillis();
        Thread.sleep(5000);
//        long end = System.currentTimeMillis();
//        System.out.println("executed time=" + (end - start) + "ms");
    }
}

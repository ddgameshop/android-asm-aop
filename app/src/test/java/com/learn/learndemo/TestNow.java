package com.learn.learndemo;

import org.junit.Test;

public class TestNow {

    @Test
    public void test(){
        System.out.println(SubClass.good);
    }
}


class SuperClass {

    static {
        System.out.println("SuperClass init");
    }
    public static int value = 10;
}

class SubClass extends SuperClass {
    static {
        System.out.println("SubClass init");
    }

    public static int good = 100;
}

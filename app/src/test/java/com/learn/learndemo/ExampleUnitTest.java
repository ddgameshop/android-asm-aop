package com.learn.learndemo;

import org.junit.Test;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
//        TestMain testMain = new TestMain();
//        testMain.doSth();
//
//
//        Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};
//        System.out.println(DEFAULTCAPACITY_EMPTY_ELEMENTDATA.length);
//        Object[] elementData = {1, 2, 3, 4, 5};
//        int index = 1;
//        int size = elementData.length;
//        int numMoved = size - index - 1;
//        if (numMoved > 0)
//            System.arraycopy(elementData, index+1, elementData, index,
//                    numMoved);
//        elementData[--size] = null;
//
//        System.out.println(elementData.length);
//
//        Map<String, String> map = new HashMap<>();
//
//        System.out.println(tableSizeFor(17));
//        System.out.println(MAXIMUM_CAPACITY);
//
//        int a = 4;
//        int b = 7;
//        System.out.println((a & b));
//        System.out.println(a % b);
//
//        System.out.println(8>>>1);
//
//
//        map.put("name", "boy");
//        map.put("name", "girl");
//        map.forEach((s, s2) -> System.out.println(s + ", value=" + s2));
//        System.out.println("hash=" + (hash("name") & 15));
//        System.out.println("hash=" + (hash("boy") & 15));
//        System.out.println("000" == "000");
//
//        Stack stack = new Stack<String>();
//        stack.push("1");
//        stack.push("2");
//        stack.push("3");
//        stack.push("4");
//        System.out.println(stack.get(2));
//
//        int[] dba = new int[10];
//        StringBuilder builder = new StringBuilder();
//        for (int i = 0; i < dba.length; i++) {
//            dba[i] = i * 2;
//            builder.append(dba[i]).append(",");
//        }
//        System.out.println(builder.toString());
//        int newIndex = binarySearch(dba, 12, 15);
//        System.out.println(newIndex);

        SortUtilsCopy sortUtils = new SortUtilsCopy();
        int[] array = {2, 3, 8, 0, 6, 9, 5, 7, 10, 1, 4};

//        sortUtils.bubbleSort(array);
//        sortUtils.selectSort(array);
//        sortUtils.insertSort(array);
        sortUtils.shellSort(array);
//        sortUtils.quickSort(array, 0, array.length - 1);

        StringBuilder builder = new StringBuilder();
        for(int item: array){
            builder.append(item).append(",");
        }
        System.out.println(builder.toString());

        Person person = new Person();
        person.work();
    }

    // This is Arrays.binarySearch(), but doesn't do any argument validation.
//    static int binarySearch(int[] array, int size, int value) {
//        int lo = 0;
//        int hi = size - 1;
//
//        while (lo <= hi) {
//            final int mid = (lo + hi) >>> 1;
//            final int midVal = array[mid];
//
//            if (midVal < value) {
//                lo = mid + 1;
//            } else if (midVal > value) {
//                hi = mid - 1;
//            } else {
//                return mid;  // value found
//            }
//        }
//        return ~lo;  // value not present


//    }

    /**
     *  二分查找
     * @param array 数组
     * @param target 需要查找的数字
     * @return 返回是的可以插入的位置
     */
    static int binarySearch(int[] array, int size, int target){
        int lo = 0;
        int hi = size - 1;
        while (lo <= hi) {
            int mid = (lo + hi) >>> 1;
            int midVal = array[mid];
            System.out.println("doing while now");
            if(target > midVal) {
                lo = mid + 1;
            } else if (target < midVal){
                hi = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    static final int MAXIMUM_CAPACITY = 1 << 30;
    static final int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }
}
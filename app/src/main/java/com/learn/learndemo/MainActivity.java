package com.learn.learndemo;

import android.Manifest;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;

import com.learn.learndemo.databinding.ActivityMainBinding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;


public class MainActivity extends AppCompatActivity {

    ObservableInt obj = new ObservableInt(2);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityMainBinding mBindView = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBindView.setLifecycleOwner(this);
        final TestVM testVM = new TestVM();
        mBindView.setViewModel(testVM);

        User user= new User();
        mBindView.setUser(user); ;

        SparseArray<String> add = new SparseArray<String>();
        add.append(4, "boy");
        System.out.println("---------"+ ","
                + add.indexOfKey(4) );

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0x001);

    }

    public class TestVM{
        public MutableLiveData<String> mLiveData = new MutableLiveData<String>();
        public MutableLiveData<String> mLiveDataboy = new MutableLiveData<String>();

        public ObservableField<String> newData = new ObservableField<>("text");
        public ObservableField<String> phone = new ObservableField<>("text");
        public TestVM(){
            mLiveData.setValue("hellow");
        }
        public void onclick(View view){
            mLiveData.setValue("boy");
            mLiveDataboy.setValue("boy");
            newData.set(String.valueOf(Math.random() + "test"));
            System.out.println("----------phone=" + phone.get());

        }

        public void go(){
//            startActivity(new Intent(TestActivity.this, TestActivity2.class));

        }
    }

}
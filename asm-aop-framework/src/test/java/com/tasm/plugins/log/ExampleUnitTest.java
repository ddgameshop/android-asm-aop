package com.tasm.plugins.log;

import com.tasm.aop.ASMEngine;
import com.tasm.aop.plugins.EventTrackPointcut;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        File classFile = new File("/Users/chendennies/Documents/eclipsespace/Android-ASM-AOP/asm-aop-framework/build/intermediates/javac/debugUnitTest/classes/com/tasm/plugins/log/TestClassNumberOne.class");
        InputStream is;
        try {
            is = new FileInputStream(classFile);
            byte[] tBytes = ASMEngine.doAspect("com.tasm.plugins.log", is);
            is.close();
            FileOutputStream fout = new FileOutputStream(classFile);
            fout.write(tBytes);
            fout.flush();
            fout.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        ASMEngine.setmOnEventTrackCallback(new EventTrackPointcut.OnEventTrackCallback() {
            @Override
            public void onEventTrackCallback(int what, String value1, String value2) {
                System.out.println("onEventTrackCallback what=" + what + " value1=" + value1 + " value2=" + value2);
            }
        });
        TestClassNumberOne one = new TestClassNumberOne();
        one.doTestFunction();
        one.doTestFunction();
    }


}
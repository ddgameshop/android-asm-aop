package com.tasm.plugins.log;

import com.tasm.aop.annotation.EventTrack;
import com.tasm.aop.annotation.InsertLog;
import com.tasm.aop.annotation.SingleClick;

import org.junit.Test;

/**
 * @author Viyu
 * @desc 测试类
 */
public class TestClassNumberOne {

    @InsertLog
    @SingleClick(value = 2400)
    @EventTrack(what = 1, value1 = "12", value2 = "23")
    public void doTestFunction() {
        System.out.println("do test function 2....");
    }

    @Test
    public static void main(String[] args) {
        System.out.println("");
    }
}

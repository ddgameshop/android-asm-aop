package com.tasm.aop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface InsertLog {
    /**
     * 打印日志的tag，默认值是InsertLog
     * @return
     */
    String TAG() default "ASPECT_LOG";
}
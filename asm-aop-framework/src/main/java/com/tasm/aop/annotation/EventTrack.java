package com.tasm.aop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * desc   : 埋点事件触发注解
 * author : chendaning
 * date   : 6/11/21
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface EventTrack {
    /**
     * 事件信息1
     * @return
     */
    String value1() default "";

    /**
     * 事件信息2
     * @return
     */
    String value2() default "";

    /**
     * 可以用于定义事件值
     * @return
     */
    int what() default 0;
}
package com.tasm.aop.framework;


import com.tasm.aop.plugins.AopPointcut;

import org.objectweb.asm.AnnotationVisitor;

/**
 * desc   : 模版
 * author : chendaning
 * date   : 6/18/21
 */
public class ASMAnnotationVisitor extends AnnotationVisitor {
    private String mAnnotationPath;
    public ASMAnnotationVisitor(int api, String annotationPath, AnnotationVisitor annotationVisitor) {
        super(api, annotationVisitor);
        this.mAnnotationPath = annotationPath;
    }

    @Override
    public void visit(String key, Object value) {
        super.visit(key, value);
        AopPointcut.annotationParam.put(mAnnotationPath + "_" + key, value);
    }

    @Override
    public void visitEnum(String s, String s1, String s2) {
        super.visitEnum(s, s1, s2);
        System.out.println();
    }

    @Override
    public AnnotationVisitor visitAnnotation(String s, String s1) {
        return super.visitAnnotation(s, s1);
    }

    @Override
    public AnnotationVisitor visitArray(String s) {
        return super.visitArray(s);
    }


    @Override
    public void visitEnd() {
        super.visitEnd();
    }
}

package com.tasm.aop.plugins;


import com.tasm.aop.ASMEngine;

public class SingleClickPointcut extends AopPointcut {
    private long value;

    public SingleClickPointcut(String className, String methodName, String annotation, long value) {
        super(className, methodName, annotation);
        this.value = value;
    }

    @Override
    public void aspectBeforeInvoke() {
        String key = mClassName + "_" + mMethodName;
        if(ASMEngine.mSingleClickTime.containsKey(key)) {
            if(System.currentTimeMillis() - ASMEngine.mSingleClickTime.get(key) <= value) {
                System.out.println("SingleClickPointcut click interval too short! value=" + value);
                return;
            }
        }
        ASMEngine.mSingleClickTime.put(key, System.currentTimeMillis());
    }

    @Override
    public void aspectAfterInvoke() {

    }

}

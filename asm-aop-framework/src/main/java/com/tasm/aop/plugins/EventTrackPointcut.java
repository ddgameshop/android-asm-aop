package com.tasm.aop.plugins;


import com.tasm.aop.ASMEngine;

public class EventTrackPointcut extends AopPointcut {


    private int what;
    private String value1;
    private String value2;
    public EventTrackPointcut(String className, String methodName, String annotation,
                              int what, String value1, String value2) {
        super(className, methodName, annotation);
        this.what = what;
        this.value1 = value1;
        this.value2 = value2;
    }

    @Override
    public void aspectBeforeInvoke() {
        if(ASMEngine.getmOnEventTrackCallback() != null) {
            ASMEngine.getmOnEventTrackCallback().onEventTrackCallback(what, value1, value2);
        }
    }

    @Override
    public void aspectAfterInvoke() {

    }

    public interface OnEventTrackCallback{
        void onEventTrackCallback(int what, String value1, String value2);
    }
}

package com.tasm.aop.plugins;

import com.tasm.aop.annotation.EventTrack;
import com.tasm.aop.annotation.InsertLog;
import com.tasm.aop.annotation.SingleClick;

import java.util.HashMap;
import java.util.Map;


public abstract class AopPointcut {

    /**
     * 类名
     */
    protected final String mClassName;

    /**
     * 方法名
     */
    protected final String mMethodName;
    /**
     * 注解名
     */
    protected final String mAnnotationName;
    /**
     * 注解的参数
     */
    public static Map<String, Object> annotationParam = new HashMap<>();


    protected AopPointcut(String className, String methodName, String annotation) {
        this.mClassName = className;
        this.mMethodName = methodName;
        this.mAnnotationName = annotation;
    }

    /**
     * 方法开始前执行的逻辑
     */
    public abstract void aspectBeforeInvoke();

    /**
     * 方法结束后执行的逻辑
     */
    public abstract void aspectAfterInvoke();

    /**
     * 生成一个Pointcut
     *
     * @param className
     * @param methodName
     * @return
     */
    public static AopPointcut newInvokerLog(String className, String methodName, String annotation) {
        return new InsertlogPointcut(className, methodName, annotation);
    }

    public static AopPointcut newInvokerEventTrack(String className, String methodName, String annotation,
                                                   int what, String value1, String value2) {
        return new EventTrackPointcut(className, methodName, annotation, what, value1, value2);
    }

    public static AopPointcut newInvokerSingleClick(String className, String methodName, String annotation, long value) {
        return new SingleClickPointcut(className, methodName, annotation, value);

//        if (annotation.equals(InsertLog.class.getName())) {
//            return new InsertlogPointcut(className, methodName, annotation);
//        } else if(annotation.equals(EventTrack.class.getName())){
//
//        } else if(annotation.equals(SingleClick.class.getName())){
//            return new SingleClickPointcut(className, methodName, annotation);
//        } else {
//            return new NullPointcut(className, methodName, annotation);
//        }
    }

    public static AopPointcut newInvoker(String className, String methodName, String annotation) {
        if (annotation.equals(InsertLog.class.getName())) {
            return new InsertlogPointcut(className, methodName, annotation);
        } else if(annotation.equals(EventTrack.class.getName())){
            return new EventTrackPointcut(className, methodName, annotation, 10, "value1", "value2");
        } else if(annotation.equals(SingleClick.class.getName())){
            return new SingleClickPointcut(className, methodName, annotation, 1000);
        }
        return new NullPointcut(className, methodName, annotation);
    }


}

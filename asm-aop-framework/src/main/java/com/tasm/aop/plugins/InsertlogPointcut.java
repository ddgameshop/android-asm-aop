package com.tasm.aop.plugins;


public class InsertlogPointcut extends AopPointcut {

    private Long mStartTime;

    public InsertlogPointcut(String className, String methodName, String annotation) {
        super(className, methodName, annotation);
    }

    @Override
    public void aspectBeforeInvoke() {
        mStartTime = System.currentTimeMillis();
        System.out.println(String.format("%s.%s aspectBeforeInvoke start:%dms", mClassName, mMethodName, mStartTime));
    }

    @Override
    public void aspectAfterInvoke() {
        Long endTime = System.currentTimeMillis();
        System.out.println(String.format("%s.%s aspectAfterInvoke consume:%dms", mClassName, mMethodName, endTime - mStartTime));
    }
}

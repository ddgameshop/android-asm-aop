package com.tasm.aop.plugins;


public class NullPointcut extends AopPointcut {

    public NullPointcut(String className, String methodName, String annotation) {
        super(className, methodName, annotation);
    }

    @Override
    public void aspectBeforeInvoke() {
        System.out.println("i am null point cut start");
    }

    @Override
    public void aspectAfterInvoke() {
        System.out.println("i am null point cut end");
    }
}

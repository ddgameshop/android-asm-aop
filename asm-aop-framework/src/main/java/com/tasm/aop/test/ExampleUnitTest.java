package com.tasm.aop.test;

import com.tasm.aop.ASMEngine;
import com.tasm.aop.plugins.EventTrackPointcut;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    public static void main(String[] args) {
        new ExampleUnitTest().addition_isCorrect();
    }

    public void addition_isCorrect() {
        File classFile = new File("/Users/chendennies/Documents/eclipsespace/Android-ASM-AOP/asm-aop-framework-java/build/classes/java/main/com/tasm/aop/test/TestClassNumberOne.class");
        InputStream is;
        try {
            is = new FileInputStream(classFile);
            byte[] tBytes = ASMEngine.doAspect("com.tasm.aop.test", is);
            is.close();
            FileOutputStream fout = new FileOutputStream(classFile);
            fout.write(tBytes);
            fout.flush();
            fout.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        ASMEngine.setmOnEventTrackCallback(new EventTrackPointcut.OnEventTrackCallback() {
            @Override
            public void onEventTrackCallback(int what, String value1, String value2) {
                System.out.println("--------evet");
            }
        });
        TestClassNumberOne one = new TestClassNumberOne();
        one.doTestFunction1();
        one.doTestFunction2();
        one.doTestFunction2();
    }


}
package com.tasm.aop.test;

import com.tasm.aop.annotation.EventTrack;
import com.tasm.aop.annotation.InsertLog;
import com.tasm.aop.annotation.SingleClick;


/**
 * @author Viyu
 * @desc 测试类
 */
public class TestClassNumberOne {

    @InsertLog
    public void doTestFunction1() {
        System.out.println("do test function.bb...");
    }

    @SingleClick(value = 2500)
    @EventTrack(what = 1, value1 = "12", value2 = "23")
    public void doTestFunction2() {
        System.out.println("do test function 2....");
        boy(12, 34);
    }

    private void boy(int age, long time){
        System.out.println();
    }


    public static void main(String[] args) {
        System.out.println("");
    }
}

# ASM AOP应用框架
这是一个轻量级使用ASM实现的AOP应用框架，简单使用注解的方式即能完成接入，简单易用。该框架可以读去class文件，读取字节码，执行搜索AOP的注解，之后插入相应的功能代码，从而达注解就能实现的AOP功能，已实现的功能参考功能里列表。基于本框架，作者开发一个andriod应用插件，android在打包app时自动完成AOP注解功能，项目地址：[ASM-AOP-Gradle-Plugin](https://gitee.com/ddgameshop/asm-aop-gradle-plugin)
## 功能列表
* 埋点事件切面 
* 自动插入日志打印切面 
* 防快速点击事件切面 
## 测试方式
clone代码，进入asm-aop-framework目录，参照ExampleUnitTest中的测试例子，这个例子就是读取指定路径的class文件，之后对该文件进行AOP切面功能，最后讲织入代码后的文件重新保存，新的文件执行，织入的代码就会得到执行，右键执行ExampleUnitTest即可（注意：执行前，记得将class文件的路径改成你自己的路径）
## 本框架接入步骤
### 1、根目录的build.gradle添加下面依赖

* 1）项目工程根目录，在根目录build.gradle文件中添加jitpack.io仓库

```
buildscript {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```
* 2）项目中的app目录的build.gradle，添加依赖

```
dependencies {
    ...
    implementation 'com.gitee.ddgameshop:android-asm-aop:v1.0.6'
}
```
* 3）添加完毕，可以开始使用该框架实现自己的业务

### 2、埋点事件切面应用
* 方法上添加注解EventTrack，
* 参数：what=整型类型数值，value1=字符串1，value2=字符串2，这两个参数都会回调中原样返回，用于处理具体的业务逻辑
* 在Application中初始化AndroidAOP的时候，设置事件埋点回调，这里完成埋点的逻辑，例如：数据上报、数据记录等等
* 方法在被调用的时候，会回调Application设置的回调接口（过滤日志关键字“ASPECT_LOG”）
* 代码实例如下

```
ASMEngine.setmOnEventTrackCallback(new EventTrackPointcut.OnEventTrackCallback() {
    @Override
    public void onEventTrackCallback(int what, String value1, String value2) {
        Toast.makeText(MainActivity.this, "what=" + what + ", value1=" + value1, Toast.LENGTH_LONG).show();
    }
});
```

```
@EventTrack(what = 12, value1 = "参数1", value2 = "参数2")
public void eventTrack(View view) {
    Log.i(TAG, "this is a EventTrack click log.");
}
```
 
### 3、自动插入日志打印切面 
* 方法上添加注解InsertLog
* 参数：TAG=设置日志打印时的tag字符串，该值是缺省的，默认值是“ASPECT_LOG”
* 方法在被调用的时候，切面会自动构造该方法的日志信息进行打印，打印的信息包括：类名、方法名、参数、返回值、执行时间，（过滤日志关键字“ASPECT_LOG”，或者自定义的tag）
* 代码实例如下


```
@InsertLog
public int getPersonalInfo(int age, String name) {
    try {
        Thread.sleep(100);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    return age + 20;
}
```
### 4、防快速点击事件切面 
* 方法上添加注解SingleClick
* 参数：value=点击时间间隔
* 方法在被调用的时候，切面会记录上次点击的时间，如果未达到设置的间隔时间，会业务打断逻辑
* 代码实例如下

```
@SingleClick(value = 4000)
public void singleClick(View view) {
    Log.i(TAG, "this is a single click log.");
    ++count;
    msg.setText("count=" + count);
}
```

## 关于我
学习在出不在入